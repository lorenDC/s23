// MAIN ACTIVITY:
// WDC028v1.5b-23 | JavaScript - Objects
// Graded Activity:
// Part 1: 
//     1. Initialize/add the following trainer object properties:
//       Name (String)
//       Age (Number)
//       Pokemon (Array)
//       Friends (Object with Array values for properties)
//     2. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
//     3. Access the trainer object properties using dot and square bracket notation.
//     4. Invoke/call the trainer talk object method.


// Part 2:

// 1.) Create a new set of pokemon for pokemon battle. (Same as our discussion)
//     - Solve the health of the pokemon that when tackle is invoked, current value of target's health should decrease continuously as many times the tackle is invoked.
//     (target.health - this.attack)

// 2.) If health is less than or equal to 5, invoke faint function


// Code Here:
// Trainer


function Trainer(trainerName, age) {

    // Properties
    this.name = trainerName
    this.age = age
    this.pokemon = ["Snorlax", "Charmander"]
    this.friends = {
        name: ["Jessie", "James"],
        gender: ["Girl", "Boy"]
    }

    // Methods
    this.talk = function() {
        console.log("Pikachu I choose you!")
    }
}

let brock = new Trainer("brock", 25)
console.log(brock)
brock.talk()

// Pokemon

function Pokemon(name, level) {

    // Properties
    this.name = name
    this.level = level
    this.health = 2 * level
    this.attack = 2 * level

    // Methods
    this.tackle = function(target) {
        console.log(this.name + " tackled "  + target.name)
        console.log(target.name + "'s health is now reduced " + (target.health - this.attack))
        newTargetHealth = target.health - this.attack;
        if (newTargetHealth <= 5) {
            target.faint();
            return target.health = newTargetHealth;
        } else {
            return target.health = newTargetHealth;
        }
    },
    this.faint = function() {
        console.log(this.name + " fainted")
    }

}

let snorlax = new Pokemon("Snorlax", 10)
let charmander = new Pokemon("Charmander", 20)

console.log(snorlax)
console.log(charmander)

charmander.tackle(snorlax)




